/************************************
    * Class      : AccountTriggerHandler
    * Test Class : AccountTriggerHandlerTest
************************************/
public class AccountTriggerHandler
{
    
    /************************************
    * Method: SendEmailAfterCustomerInactive
    * Parameters: Map<Id, Account>
    * Return Type: Null
    ************************************/
    public static void SendEmailAfterCustomerInactive(Map<Id, Account> mapAccountOld, Map<Id, Account> mapAccountNew)
    {

        /* the following was inserted by apex_changer.py */
        //Integer ferretSeqNumber = FerretLogGeneratorV1.getSequenceNumber();
        try {
        //FerretLogGeneratorV1.logMethodEntry('AccountTriggerHandler', 'SendEmailAfterCustomerInactive(x2)');
        /* do not add code inside this block */


        List<Account> lstAccountInactive = new List<Account>();
        List<Account> lstAccountTerminated = new List<Account>();
        List<Account> lstAccountPurchased = new List<Account>();
        List<Account> lstAccountBillingAddress = new List<Account>();
        List<Account> lstAccountShippingAddress = new List<Account>();        
        List<Account> lstAccountAutoPayChecked = new List<Account>();
        List<Account> lstAccountAutoPayUnChecked = new List<Account>();        
        List<Messaging.SingleEmailMessage> lstSingleEmailMessage = new List<Messaging.SingleEmailMessage>();
        Map<String, EmailTemplate> MapEmailTemplateByDevName = new Map<String, EmailTemplate>();
        Set<string> setEmailTemplateDeveloperName = new set<String>();
        Id OrgWideEmailAddressId;
        
        
        setEmailTemplateDeveloperName.add(system.label.AccountStatusInactive);
        setEmailTemplateDeveloperName.add(system.label.AccountStatusTerminated);
        setEmailTemplateDeveloperName.add(system.label.AccountStatusPurchased);
        setEmailTemplateDeveloperName.add(system.label.AccPrimaryAddressModified);
        setEmailTemplateDeveloperName.add(system.label.ShippingAddressModified);        
        setEmailTemplateDeveloperName.add(system.label.AutoPayment);
        setEmailTemplateDeveloperName.add(system.label.AutoPaymentUnchecked);
        
        /* List for Sending Email 
        List<MC_sendMCNotification__c> lstSendEmail = new List<MC_sendMCNotification__c>();
        
        MC_sendMCNotification__c objSendEmail;*/
               
        List<EmailTemplate> lstEmailTemplate = new List<EmailTemplate>([SELECT Id, DeveloperName FROM EmailTemplate where DeveloperName IN: setEmailTemplateDeveloperName]);        
        
        List<OrgWideEmailAddress> lstOrgWideEmailAddress = new List<OrgWideEmailAddress>([select id, Address from OrgWideEmailAddress where Address=: System.Label.OrgWideEmailAddress]);
        
        if(lstOrgWideEmailAddress.size() > 0)
            OrgWideEmailAddressId = lstOrgWideEmailAddress[0].Id;
        
        for(EmailTemplate EmailTemplateNew :lstEmailTemplate)
        {
            MapEmailTemplateByDevName.put(EmailTemplateNew.DeveloperName, EmailTemplateNew);
        }
        
        // MC Notifications
       // Set<Id> setContactID = new Set<Id>();
        
        /* MC Notifications
        for(Account accountOld : mapAccountOld.values())
        {
            setContactID.add(mapAccountNew.get(accountOld.Id).vlocity_cmt__PrimaryContactId__c);
        }
        
        // MC Notifications
        Map<Id,String> mapContactIDEmail = new Map<Id,String>();
        List<Contact> lstContactDetails = [Select id,Email from Contact where id in :setContactID];
        
        system.debug('!!!lstContactDetails!!!'+lstContactDetails);
        
        // MC Notifications
        for (Contact objContactDetails: lstContactDetails){
            mapContactIDEmail.put(objContactDetails.id,objContactDetails.Email);
        }
        
        system.debug('!!!! mapContactIDEmail !!!!'+mapContactIDEmail);*/
        
        
        for(Account accountOld : mapAccountOld.values())
        {
           
        
            if(accountOld.vlocity_cmt__Status__c != mapAccountNew.get(accountOld.Id).vlocity_cmt__Status__c && (system.label.Inactive).equalsIgnoreCase(mapAccountNew.get(accountOld.Id).vlocity_cmt__Status__c ))
            {
                //Create a record in the Send Email object   
                // MC Notifications
                /* Initialize the sendEmail object record
                objSendEmail = new MC_sendMCNotification__c();
                objSendEmail.MC_emailType__c = 'Account Inactive';
                system.debug('!!!! Email !!! '+mapAccountNew.get(accountOld.Id).vlocity_cmt__PrimaryContactId__c);
                system.debug('!!!! mapContactIDEmail.get(accountOld.Id) !!!!' + mapContactIDEmail.get(accountOld.Id));                
                objSendEmail.MC_subscriberEmailId__c = mapContactIDEmail.get(mapAccountNew.get(accountOld.Id).vlocity_cmt__PrimaryContactId__c);
                lstSendEmail.add(objSendEmail);*/
                
                
                lstAccountInactive.add(mapAccountNew.get(accountOld.Id));
                
                
            }
            
            if(accountOld.vlocity_cmt__Status__c != mapAccountNew.get(accountOld.Id).vlocity_cmt__Status__c && (system.label.Terminated).equalsIgnoreCase(mapAccountNew.get(accountOld.Id).vlocity_cmt__Status__c ))
            {
                //SG - Start
                /* Initialize the sendEmail object record
                objSendEmail = new MC_sendMCNotification__c();
                objSendEmail.MC_emailType__c = 'Account Terminated';
                system.debug('!!!! Email !!! '+mapAccountNew.get(accountOld.Id).vlocity_cmt__PrimaryContactId__c);
                system.debug('!!!! mapContactIDEmail.get(accountOld.Id) !!!!' + mapContactIDEmail.get(accountOld.Id));                
                objSendEmail.MC_subscriberEmailId__c = mapContactIDEmail.get(mapAccountNew.get(accountOld.Id).vlocity_cmt__PrimaryContactId__c);
                lstSendEmail.add(objSendEmail);
                */
                lstAccountTerminated.add(mapAccountNew.get(accountOld.Id));
            }
            
            if(accountOld.vlocity_cmt__Status__c != mapAccountNew.get(accountOld.Id).vlocity_cmt__Status__c && (system.label.Purchased).equalsIgnoreCase(mapAccountNew.get(accountOld.Id).vlocity_cmt__Status__c ))
            {
                //SG - Start
                /* Initialize the sendEmail object record
                objSendEmail = new MC_sendMCNotification__c();
                objSendEmail.MC_emailType__c = 'Account Purchased';
                system.debug('!!!! Email !!! '+mapAccountNew.get(accountOld.Id).vlocity_cmt__PrimaryContactId__c);
                system.debug('!!!! mapContactIDEmail.get(accountOld.Id) !!!!' + mapContactIDEmail.get(accountOld.Id));                
                objSendEmail.MC_subscriberEmailId__c = mapContactIDEmail.get(mapAccountNew.get(accountOld.Id).vlocity_cmt__PrimaryContactId__c);
                lstSendEmail.add(objSendEmail);
                */
                lstAccountPurchased.add(mapAccountNew.get(accountOld.Id));
            }
            
            if((accountOld.BillingStreet != mapAccountNew.get(accountOld.Id).BillingStreet && String.isNotBlank(accountOld.BillingStreet)) ||
               (accountOld.BillingCity != mapAccountNew.get(accountOld.Id).BillingCity && String.isNotBlank(accountOld.BillingCity)) ||
               (accountOld.BillingState != mapAccountNew.get(accountOld.Id).BillingState && String.isNotBlank(accountOld.BillingState)) ||
               (accountOld.BillingPostalCode != mapAccountNew.get(accountOld.Id).BillingPostalCode && String.isNotBlank(accountOld.BillingPostalCode)) ||
               (accountOld.BillingCountry != mapAccountNew.get(accountOld.Id).BillingCountry && String.isNotBlank(accountOld.BillingCountry)))
            {
                //SG - Start
                /* Initialize the sendEmail object record
                objSendEmail = new MC_sendMCNotification__c();
                objSendEmail.MC_emailType__c = 'Primary Address Modified';
                system.debug('!!!! Email !!! '+mapAccountNew.get(accountOld.Id).vlocity_cmt__PrimaryContactId__c);
                system.debug('!!!! mapContactIDEmail.get(accountOld.Id) !!!!' + mapContactIDEmail.get(accountOld.Id));                
                objSendEmail.MC_subscriberEmailId__c = mapContactIDEmail.get(mapAccountNew.get(accountOld.Id).vlocity_cmt__PrimaryContactId__c);
                lstSendEmail.add(objSendEmail);
                */
                lstAccountBillingAddress.add(mapAccountNew.get(accountOld.Id));
            }
            
            if((accountOld.ShippingStreet != mapAccountNew.get(accountOld.Id).ShippingStreet && String.isNotBlank(accountOld.ShippingStreet)) ||
               (accountOld.ShippingCity != mapAccountNew.get(accountOld.Id).ShippingCity && String.isNotBlank(accountOld.ShippingCity)) ||
               (accountOld.ShippingState != mapAccountNew.get(accountOld.Id).ShippingState && String.isNotBlank(accountOld.ShippingState)) ||
               (accountOld.ShippingPostalCode != mapAccountNew.get(accountOld.Id).ShippingPostalCode && String.isNotBlank(accountOld.ShippingPostalCode)) ||
               (accountOld.ShippingCountry != mapAccountNew.get(accountOld.Id).ShippingCountry && String.isNotBlank(accountOld.ShippingCountry)))
            {
                //SG - Start
                /* Initialize the sendEmail object record
                objSendEmail = new MC_sendMCNotification__c();
                objSendEmail.MC_emailType__c = 'Shipping Address Modified';
                system.debug('!!!! Email !!! '+mapAccountNew.get(accountOld.Id).vlocity_cmt__PrimaryContactId__c);
                system.debug('!!!! mapContactIDEmail.get(accountOld.Id) !!!!' + mapContactIDEmail.get(accountOld.Id));                
                objSendEmail.MC_subscriberEmailId__c = mapContactIDEmail.get(mapAccountNew.get(accountOld.Id).vlocity_cmt__PrimaryContactId__c);
                lstSendEmail.add(objSendEmail);
                */
                lstAccountShippingAddress.add(mapAccountNew.get(accountOld.Id));
            }
            
            if(accountOld.vlocity_cmt__EnableAutopay__c != mapAccountNew.get(accountOld.Id).vlocity_cmt__EnableAutopay__c)
            {
                if(mapAccountNew.get(accountOld.Id).vlocity_cmt__EnableAutopay__c)
                    {
                    //SG - Start
                    /* Initialize the sendEmail object record
                    objSendEmail = new MC_sendMCNotification__c();
                    objSendEmail.MC_emailType__c = 'Auto-pay Checked';
                    system.debug('!!!! Email !!! '+mapAccountNew.get(accountOld.Id).vlocity_cmt__PrimaryContactId__c);
                    system.debug('!!!! mapContactIDEmail.get(accountOld.Id) !!!!' + mapContactIDEmail.get(accountOld.Id));                
                    objSendEmail.MC_subscriberEmailId__c = mapContactIDEmail.get(mapAccountNew.get(accountOld.Id).vlocity_cmt__PrimaryContactId__c);
                    lstSendEmail.add(objSendEmail);
                    */
                    lstAccountAutoPayChecked.add(mapAccountNew.get(accountOld.Id));
                    }
                else
                    {
                    //SG - Start
                    /* Initialize the sendEmail object record
                    objSendEmail = new MC_sendMCNotification__c();
                    objSendEmail.MC_emailType__c = 'Auto-pay Unchecked';
                    system.debug('!!!! Email !!! '+mapAccountNew.get(accountOld.Id).vlocity_cmt__PrimaryContactId__c);
                    system.debug('!!!! mapContactIDEmail.get(accountOld.Id) !!!!' + mapContactIDEmail.get(accountOld.Id));                
                    objSendEmail.MC_subscriberEmailId__c = mapContactIDEmail.get(mapAccountNew.get(accountOld.Id).vlocity_cmt__PrimaryContactId__c);
                    lstSendEmail.add(objSendEmail);
                    */
                    lstAccountAutoPayUnChecked.add(mapAccountNew.get(accountOld.Id));
                    }
            }
            
        }
        
        // MC Notifications
        
        //COMMENTING THIS CODE as it's giving following exception while updating account. Please fix this issue.
        //Before Insert or Upsert list must not have two identically equal elements
       // insert lstSendEmail; 
      if(!System.isBatch() && !System.isFuture()){  
        if(lstAccountInactive.size() == 1)
        {
            utils.sendSMS(lstAccountInactive[0].Id, system.label.DeactivateServiceMSG);
        }
        if(lstAccountTerminated.size() == 1)
        {
            utils.sendSMS(lstAccountTerminated[0].Id, system.label.DeactivateServiceMSG);
        }
        if(lstAccountPurchased.size() == 1)
        {
            utils.sendSMS(lstAccountPurchased[0].Id, system.label.PurchaseMSG);
        }
        if(lstAccountBillingAddress.size() == 1)
        {
            utils.sendSMS(lstAccountBillingAddress[0].Id, system.label.BillingAddrChangeMSG);
        }
        if(lstAccountShippingAddress.size() == 1)
        {
            utils.sendSMS(lstAccountShippingAddress[0].Id, system.label.ShippingAddrChangeMSG);
        }        
        if(lstAccountAutoPayChecked.size() == 1)
        {
            utils.sendSMS(lstAccountAutoPayChecked[0].Id, system.label.AutoPayMSG);
        }
        if(lstAccountAutoPayUnChecked.size() == 1)
        {
            utils.sendSMS(lstAccountAutoPayUnChecked[0].Id, system.label.ManualPayMSG);
        }
        }        
        EmailTemplate EmailTemplateInactive = MapEmailTemplateByDevName.get(system.label.AccountStatusInactive);
        
        if(lstAccountInactive.size() > 0 &&  EmailTemplateInactive != null)
        {
            List<Messaging.SingleEmailMessage> lstSingleEmailMessageInactive = getSingleEmailMessageList(lstAccountInactive,EmailTemplateInactive.Id,OrgWideEmailAddressId);
            lstSingleEmailMessage.AddAll(lstSingleEmailMessageInactive);
        }
        
        EmailTemplate EmailTemplateTerminated = MapEmailTemplateByDevName.get(system.label.AccountStatusTerminated);
        if(lstAccountTerminated.size() > 0 &&  EmailTemplateTerminated != null)
        {
            List<Messaging.SingleEmailMessage> lstSingleEmailMessageTerminated = getSingleEmailMessageList(lstAccountTerminated,EmailTemplateTerminated.Id,OrgWideEmailAddressId);
            lstSingleEmailMessage.AddAll(lstSingleEmailMessageTerminated);
        }
        
        EmailTemplate EmailTemplatePurchased = MapEmailTemplateByDevName.get(system.label.AccountStatusPurchased);
        if(lstAccountPurchased.size() > 0 &&  EmailTemplatePurchased != null)
        {
            List<Messaging.SingleEmailMessage> lstSingleEmailMessagePurchased = getSingleEmailMessageList(lstAccountPurchased,EmailTemplatePurchased.Id,OrgWideEmailAddressId);
            lstSingleEmailMessage.AddAll(lstSingleEmailMessagePurchased);
        }
        
        EmailTemplate EmailTemplateBillingAddress = MapEmailTemplateByDevName.get(system.label.AccPrimaryAddressModified);
        if(lstAccountBillingAddress.size() > 0 &&  EmailTemplateBillingAddress != null)
        {
            List<Messaging.SingleEmailMessage> lstSingleEmailMessageBilling = getSingleEmailMessageList(lstAccountBillingAddress,EmailTemplateBillingAddress.Id,OrgWideEmailAddressId);
            lstSingleEmailMessage.AddAll(lstSingleEmailMessageBilling);
        }
        
        EmailTemplate EmailTemplateShippingAddress = MapEmailTemplateByDevName.get(system.label.ShippingAddressModified);
        if(lstAccountShippingAddress.size() > 0 && EmailTemplateShippingAddress != null)
        {
            List<Messaging.SingleEmailMessage> lstSingleEmailMessageShipping = getSingleEmailMessageList(lstAccountShippingAddress,EmailTemplateShippingAddress.Id,OrgWideEmailAddressId);
            lstSingleEmailMessage.AddAll(lstSingleEmailMessageShipping);
        }
        
        EmailTemplate EmailTemplateAutoPayChecked = MapEmailTemplateByDevName.get(system.label.AutoPayment);
        if(lstAccountAutoPayChecked.size() > 0 && EmailTemplateAutoPayChecked != null)
        {
            List<Messaging.SingleEmailMessage> lstSingleEmailMessageAutoPayChecked = getSingleEmailMessageList(lstAccountAutoPayChecked,EmailTemplateAutoPayChecked.Id,OrgWideEmailAddressId);
            lstSingleEmailMessage.AddAll(lstSingleEmailMessageAutoPayChecked);
        }
        
        EmailTemplate EmailTemplateAutoPayUnChecked = MapEmailTemplateByDevName.get(system.label.AutoPaymentUnchecked);
        if(lstAccountAutoPayUnChecked.size() > 0 && EmailTemplateAutoPayUnChecked != null)
        {
            List<Messaging.SingleEmailMessage> lstSingleEmailMessageAutoPayUnChecked = getSingleEmailMessageList(lstAccountAutoPayUnChecked,EmailTemplateAutoPayUnChecked.Id,OrgWideEmailAddressId);
            lstSingleEmailMessage.AddAll(lstSingleEmailMessageAutoPayUnChecked);
        }
        
        if(lstSingleEmailMessage.size() > 0)
        {
            //Utils.sendEmail(lstSingleEmailMessage);
        }
    

        /* the following was inserted by apex_changer.py */
        } finally {
        //FerretLogGeneratorV1.logMethodExit('AccountTriggerHandler', 'SendEmailAfterCustomerInactive(x2)', ferretSeqNumber);
        }
        /* do not add code inside this block */

}
    
     /************************************
    * Method: getSingleEmailMessageList
    * Parameters:List<Account>,Id,Id
    * Return Type: List<Messaging.SingleEmailMessage>
    ************************************/
    public static List<Messaging.SingleEmailMessage> getSingleEmailMessageList(List<Account> lstAccount, Id TemplateId, Id OrgWideEmailAddressId)
    {

        /* the following was inserted by apex_changer.py */
        //Integer ferretSeqNumber = FerretLogGeneratorV1.getSequenceNumber();
        try {
        //FerretLogGeneratorV1.logMethodEntry('AccountTriggerHandler', 'getSingleEmailMessageList(x3)');
        /* do not add code inside this block */


        List<Messaging.SingleEmailMessage> lstSingleEmailMessage = new List<Messaging.SingleEmailMessage>();
        
        for(Account account :lstAccount)
        {
            if(account.vlocity_cmt__PersonContactId__c != null)
            {
                Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
                message.setTargetObjectId(account.vlocity_cmt__PersonContactId__c);
                message.setTemplateId(TemplateId);
                message.setWhatId(account.Id);
                message.setSaveAsActivity(false);
                if(OrgWideEmailAddressId != null)
                    message.setOrgWideEmailAddressId(OrgWideEmailAddressId);
                lstSingleEmailMessage.add(message);
            }
        }
        return lstSingleEmailMessage;
    

        /* the following was inserted by apex_changer.py */
        } finally {
        //FerretLogGeneratorV1.logMethodExit('AccountTriggerHandler', 'getSingleEmailMessageList(x3)', ferretSeqNumber);
        }
        /* do not add code inside this block */

}
   // Commented out and fix is in GeocodeblockonAccount for VZWP-2383. Vishal Kumar 
  /* public static void genrateAutoNumber(List<Account> listNew){
            Integer counter =0;
            system.debug('counter init'+counter );
            List<Account> accLst = [SELECT id,CustomerNumber__c,Customer_Number__c  from account ORDER BY createdDate DESC limit 1];                              
            if(accLst  != null && accLst.size() > 0){  
                for(Account acc : listNew){
                           counter++;
                           system.debug('counter loop'+counter );
                           if(!Test.isrunningTest()){
                               acc.Customer_Number__c = String.valueOf(Integer.valueOf(accLst[0].Customer_Number__c )+ counter).leftPad(12, label.Label_0);
                           }
                }   
            }             
   }*/
    public static void changeFraudCheck(List<Account> listOld, List<Account> listNew){

        /* the following was inserted by apex_changer.py */
        //Integer ferretSeqNumber = FerretLogGeneratorV1.getSequenceNumber();
        try {
        //FerretLogGeneratorV1.logMethodEntry('AccountTriggerHandler', 'changeFraudCheck(x2)');
        /* do not add code inside this block */


        Set<Id> accIdSet = new Set <Id>();
        if(listOld.size()>0 && listNew.size()>0){
           for(Account acc: listNew){
               accIdSet.add(acc.id);
           }
           if(accIdSet != null){
               List <OrderItem> ordItemList = [Select Id, Fraud_Status__c, Order.Account.Fraud_Customer__c, Product2.Family,OrderAccountId__c from OrderItem WHERE (Order.AccountId IN: accIdSet AND Order.Status =: label.label_Hold  AND Product2.Family =: label.PRODUCTFAMILY_HANDSET ) ORDER BY CREATEDDATE DESC Limit 1];
               if(ordItemList.size()>0 && ordItemList != null){
                   for(OrderItem odrItem : ordItemList) {
                       odrItem.Fraud_Status__c = odrItem.order.Account.Fraud_Customer__c;
                   }
                   update ordItemList;
               }
           }
        }
    

        /* the following was inserted by apex_changer.py */
        } finally {
        //FerretLogGeneratorV1.logMethodExit('AccountTriggerHandler', 'changeFraudCheck(x2)', ferretSeqNumber);
        }
        /* do not add code inside this block */

    }
    
    public static void referralNumber(List<Account> oldList, List<Account> newList){
        system.debug('Value of oldList : '+oldList);
        system.debug('Value of newList : '+newList);
        set<string> accSet = new set<string>();
        if(oldList.size()>0 && newList.size()>0){
            for(account acc: newList){
                accSet.add(acc.id);
            }
            Account oldAcc = oldList[0];
            Account newAcc = newList[0];
            system.debug('Value of oldAccStatus: '+oldAcc.vlocity_cmt__Status__c);
            system.debug('Value of newAccStatus: '+newAcc.vlocity_cmt__Status__c);
            if((oldAcc.vlocity_cmt__Status__c != null && oldAcc.vlocity_cmt__Status__c != null)&&(oldAcc.vlocity_cmt__Status__c.equalsIgnoreCase(label.Label_Purchased) || oldAcc.vlocity_cmt__Status__c.equalsIgnoreCase(label.label_PreActive))  && newAcc.vlocity_cmt__Status__c == Label.Label_Active){
                String code = RandomReferralNumber.randomNumber();
                system.debug('Value of Code : '+code);
                if(!String.isBlank(code)){
                   Referral_Code_Detail__c rfCode = new Referral_Code_Detail__c();
                   rfCode.Referral_Code__c = code;
                   rfCode.accountid__c = oldAcc.id;
                   system.debug('Inside If');
                   insert rfCode;
                }
            }
        }
    }
    
    public static void validateAccountStatus(map<id,account> oldAccountMap,map<id,account> newAccountMap){
         
         
         for(Account oldAcc: oldAccountMap.values()){
         
            if((oldAcc.vlocity_cmt__Status__c != null && (oldAcc.vlocity_cmt__Status__c == 'Active') && newAccountMap.get(oldAcc.id).vlocity_cmt__Status__c == 'PreActive') ){
                
                throw new applicationException(label.Account_status_should_not_be_changed_from_Active_to_preactive);
            }
         }
     }
     
     public static void fraudAccountTrigger(map<id,account> oldAccountMap,map<id,account> newAccountMap){
        Account oldAcc;
        Account newAcc;
        for (Id accId : newAccountMap.keySet()) {
            oldAcc = oldAccountMap.get(accId);
            newAcc = newAccountMap.get(accId);
            if (oldAcc.Fraud_Customer__c  == false && newAcc.Fraud_Customer__c == true) {
                newAcc.Fraud_Marked__c = system.now();
            }else if(oldAcc.Fraud_Customer__c  == true && newAcc.Fraud_Customer__c == false){
                newAcc.Fraud_Marked__c = null;
            }
        }
         
     }
     
     
     public static void createSuspendedAccountOrder(map<id,account> oldAccountMap,map<id,account> newAccountMap){
        Account oldAcc;
        Account newAcc;
        List<Account> accToUpdate = new List<Account>();
        List<Order> ordToUpdate = new List<Order>();
        for (Id accId : newAccountMap.keySet()) {
            oldAcc = oldAccountMap.get(accId);
            newAcc = newAccountMap.get(accId);
            if (oldAcc.CreateOrderForSuspendedAccount__c == false && newAcc.CreateOrderForSuspendedAccount__c == true) {
                List<Account> acc = [select id,vlocity_cmt__Status__c from account where account.id =:newAcc.id limit 1];
                if(acc != null && acc.size()>0){
                    for(Account a: acc){
                        a.vlocity_cmt__Status__c = label.Label_Suspended;
                        accToUpdate.add(a);
                    }
                }
                List<Order> ord = [select id,status from order where order.accountid =: newAcc.id AND order.status =:label.label_Hold limit 1];
                if(ord != null && ord.size()>0){
                    for(Order o: ord){
                        o.status = label.Label_Cancelled;
                        o.Cancellation_Reason__c = label.label_FRAUD;
                        ordToUpdate.add(o);
                    }
                }
            }
        }
        update accToUpdate;
        update ordToUpdate;
         
     }
     public class applicationException extends Exception {}

}