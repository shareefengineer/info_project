/***********
Class Name : PIChangeMDN
Test Class : PIChangeMDNTest
************/
global class PIChangeMDN {
     static string newAccountId;
     static String newMDN;
     static string newOtherCarrierAccount;
     static String newOtherCarrierPin;
     static ReturnClass returnClass = new ReturnClass();
     /***********
        Method Name : startPIChangeMDN
        Params      : String
        ReturnType  : ReturnClass 
    ***********/
  global static ReturnClass startPIChangeMDN(String accountId, String mdn,string otherCarrierAccount, String OtherCarrierPin) {
        if(string.isnotBlank(accountId)){
            newAccountId =accountId;
        }
        if(string.isnotBlank(mdn)){
             newMDN = mdn ;
        }
        if(string.isnotBlank(otherCarrierAccount)){
            newOtherCarrierAccount = otherCarrierAccount;
        }
        if(string.isnotBlank(OtherCarrierPin)){
            newOtherCarrierPin = OtherCarrierPin;
        }
        String orderId;
        if(string.isnotblank(accountId)){
         List<Order> ordList =[select id,accountid from order where accountid =:accountId and (status = 'Activated' OR status = 'Completed') limit 1];
             if(ordList != null && ordList.size() > 0){
                 //code start - //VZWDO-3880 -check for Existing Orders-danish added code to check for any order already in system.
                 List<OrderItem> OrderItemList = [SELECT Id,Order.Ordernumber,Order.status, Order.Account.vlocity_cmt__Status__c from OrderItem where Order.status != 'Cancelled' AND OrderId !=: ordList[0].id and mdn__c =: mdn];
                 OrderItem OrdItm;
                 if(OrderItemList.size() > 0 ){
                     OrdItm = OrderItemList[0];
                     if(OrdItm.Order.Account.vlocity_cmt__Status__c != 'Terminated'){
                         returnClass.message = 'Order in '+OrderItemList[0].Order.status+' status was found with same Mobile Device Number.Order number: '+OrderItemList[0].Order.Ordernumber+'. Your Order cannot be created.' ;
                         returnClass.statusCode = '400';
                         return returnClass;
                     }
                 }
                 orderId = ordList[0].id;
                 String inputJSON = generateJSON(orderId);
                 System.HttpRequest request = createHttpRequest(label.PIChangeMDN_Url, label.PIChangeMDN_Path, inputJSON);
                 Http http = new Http();
                 HttpResponse res;
                 if(!Test.isRunningTest()){
                     res = http.send(request);
                 }else{
                     MockHttpResponseGenerator mockHttpResponseGenerator =new MockHttpResponseGenerator();
                     HttpRequest reqt= new HttpRequest();
                     res=mockHttpResponseGenerator.respond(reqt);
                     res.setHeader('Content-Type', 'application/json');
                     res.setBody('{"errorCode": "0","spidOcn": "test","portEligibilityInd":"test","numberLoc":"test","":""}');
                     res.setStatusCode(200);
                 }
                 processResponse(res, orderId);
             }
             else{
                returnClass.message = 'Order not found';
                returnClass.statusCode = '400';
                return returnClass;
            }
        }
        else{
            returnClass.message = 'Account ID not found';
            returnClass.statusCode = '400';
            return returnClass;
        }
         return returnClass;
     }
     /***********
        Method Name : createHttpRequest
        Params      : String
        ReturnType  : System.HttpRequest
    ***********/
     public static System.HttpRequest createHttpRequest(String url, String path, String requestPayload) {
         HttpRequest request = new HttpRequest();
         String requestParam = url + path + '?' + getQueryParams(requestPayload);
         request.setEndpoint(requestParam);
         request.setMethod('GET');
         String authorizationHeader = Label.PIChangeMDN_AuthKey;
         request.setHeader('apikey', authorizationHeader);
         return request;
     }
     /***********
        Method Name : getQueryParams
        Params      : String
        ReturnType  : String
    ***********/
private static String getQueryParams(String jsonPayload) {
        Map<String, Object> queryParamMap = (Map<String, Object>)JSON.deserializeUntyped(jsonPayload);
        String queryParamStr='' ;
        for(string str : queryParamMap.keySet()) {
            queryParamStr = queryParamStr + str + '=' + String.valueOf(queryParamMap.get(str)) + '&';
        }
        queryParamStr.removeEnd('&');
        queryParamStr = queryParamStr + '_OFMT=xml';
        return queryParamStr;
    }
    /***********
        Method Name : generateJson
        Params      : String
        ReturnType  : String
    ***********/
    private static String generateJson(String orderId) {
        RequestETNIRCTR requestETNIRCTR = new RequestETNIRCTR ();
        requestETNIRCTR.userid = 'TA-APIGEE';
        requestETNIRCTR.applid ='T-RCTR';
        requestETNIRCTR.clientid ='test';
        
        if(Test.isRunningTest()) {
           TestDataFactory.createTestAccountRecords(3,1);
            list<Account> acc = [SELECT Id from Account];
            TestDataFactory.createTestOrderRecords(acc[0].Id);
            Order ord =[Select Id,OrderNumber from Order Limit 1 ];
            orderId = ord.Id;
        }
        //String orderId = order.getId();
        //list<orderitem> listOlitoUpdate = new list<orderitem>();
        requestETNIRCTR.mdn = newMDN;
        
            
            String jsonString;
            if(requestETNIRCTR != null){
            jsonString = JSON.serialize(requestETNIRCTR);
        }
        return jsonString;
    }
    /***********
        Method Name : processResponse
        Params      : System.HttpResponse,String 
        ReturnType  : void 
    ***********/
    public static void processResponse(System.HttpResponse res, String orderId){
        Map<String, Object> responseMap = (Map<String, Object>)JSON.deserializeUntyped(res.getBody());
        Map<String, String> response = new Map<String, String>();
         //system.debug('responsemap==='+responsemap);
        try{
            for(string str : responseMap.keySet()) {
                response.put(str, String.valueOf(responseMap.get(str)));
            }
          //  system.debug('response==='+response);
            if(response.get('errorCode').equals('0')){
                String mdnNumberLoc='';
                if(string.isnotEmpty(response.get('spidOcn')) && string.isnotEmpty(response.get('portEligibilityInd'))){
                    String mdnSpidOcn = response.get('spidOcn');    
                    String mdnPortEligibility = response.get('portEligibilityInd');
                    if(string.isnotEmpty(response.get('numberLoc'))){
                        mdnNumberLoc = response.get('numberLoc');
                    }
                    OrderCreationOnMDNChange.createorder(newAccountId, newMDN,newOtherCarrierAccount,newOtherCarrierPin,mdnSpidOcn,mdnPortEligibility,mdnNumberLoc);
                    returnClass.message = 'Success';
                    returnClass.statusCode = '200';
                }
                else{
                    returnClass.message = 'Either SpidOcn or portEligibilityInd is not returned from ETNI.';
                    returnClass.statusCode = '400';  
                }
                
            }
            else {
                returnClass.message = 'Error messsage returned from RCTR :'+''+response.get('errorMsg');
                returnClass.statusCode = '400';    
            }  
        }
        catch(exception e){
            returnClass.message = 'Internal Server Error';
            returnClass.statusCode = '500';
        }
    }
    /*********
    Wrapper class: RequestETNIRCTR 
    ********/
     global class RequestETNIRCTR {
        global String userid;
        global String applid;
        global String clientid;
        global String mdn;
    }
    /*********
    Wrapper class: ReturnClass 
    ********/
    global class ReturnClass {
        global String message;
        global String statusCode;
    }
    /**************
     Class Name : applicationException 
    ***************/
    public class applicationException extends Exception {}

}