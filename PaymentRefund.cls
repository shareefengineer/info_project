@RestResource(urlMapping='/v1/PaymentRefund')
global class PaymentRefund{
    
    @HttpPost
    global static ReturnClass paymentRefund(String ordNumber, String ordDate,String serviceOfferExtId,String returnOrdId,String reasonCode) {
        ReturnClass returnObj = new ReturnClass();
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        
        if (res == null) {
            res = new RestResponse();
            RestContext.response = res;
        }
        if(String.isBlank(ordNumber) || ordNumber == null){
            returnObj.message = label.Order_number_should_not_be_blank; 
            returnObj.statuscode = label.Label_400;
            res.responseBody = Blob.valueOf(Json.serialize(returnObj));
            res.addHeader(label.label_Content_Type, label.label_application_json);
            res.statusCode = 400;
            return returnObj ;
        }
        else{
            List<Order> finalOrder = [Select Id, Type from Order where OrderNumber =: ordNumber AND (Type=:System.Label.OrderTypeActivation OR Type ='Restore' OR Type ='Reactivate') ORDER BY CREATEDDATE DESC Limit 1];
            if(finalOrder != null && finalOrder.size() >0){
                List<OrderItem> oliLst = new List<OrderItem>([Select id, product2.family from orderItem where OrderId =: finalOrder[0].id AND product2.family = 'PLAN' AND Product2.ProductCode = 'Visible_Unlimited'  AND Refunded__c = false Limit 1]);
                if(oliLst.size() >0 && oliLst != null)
                {
                    CallMtxVIPfromCare.OrderDetail responseFromClass = CallMtxVIPfromCare.callVIP(oliLst[0].id,Label.RefundService,reasonCode);
                    returnObj.message = responseFromClass.message;
                    returnObj.statuscode = responseFromClass.statuscode  ;
                    return returnObj ;
                }
            }
        }
        return returnObj ;
       /* 
        
        
        if(String.isBlank(serviceOfferExtId) || serviceOfferExtId == null){
            returnObj.message = label.Service_offer_external_id_should_not_be_blank; 
            returnObj.statuscode = label.Label_400;
            res.responseBody = Blob.valueOf(Json.serialize(returnObj));
            res.addHeader(label.label_Content_Type, label.label_application_json);
            res.statusCode = 400;
            return returnObj ;
        }
        
        if(String.isBlank(ordDate)|| ordDate == null){
            returnObj.message = label.Order_date_should_not_be_blank; 
            returnObj.statuscode = label.Label_400;
            res.responseBody = Blob.valueOf(Json.serialize(returnObj));
            res.addHeader(label.label_Content_Type, label.label_application_json);
            res.statusCode = 400;
            return returnObj ;
        }
        
        if(String.isBlank(returnOrdId) || returnOrdId == null){
            returnObj.message = label.Return_order_number_should_not_be_blank; 
            returnObj.statuscode = label.Label_400;
            res.responseBody = Blob.valueOf(Json.serialize(returnObj));
            res.addHeader(label.label_Content_Type, label.label_application_json);
            res.statusCode = 400;
            return returnObj ;
        }
        
        try{
            Account acc = [select id,GlobalKey__c from account where id=:[select accountid from order where OrderNumber =: ordNumber OR Id =: ordNumber].accountid];
            if(acc != null){
                RefundServiceOrdrInfo RefundServiceOrderInfo = new RefundServiceOrdrInfo();
                RefundServiceOrderInfo.dollarSymbol = label.VisibleRefundServiceOrderInfo;
                if(String.isNotBlank(returnOrdId) && returnOrdId != null){
                RefundServiceOrderInfo.ReturnOrderId = returnOrdId; 
                }
                else{
                    RefundServiceOrderInfo.ReturnOrderId = '00000';
                }
                RefundServiceOrderInfo.OriginalOrderId = ordNumber;
                RefundServiceOrderInfo.ServiceOfferExternalId = serviceOfferExtId;
                RefundServiceOrderInfo.OriginalOrderDate = ordDate;
                
                
                MatrixxPaymentRefundInfo matrixxPaymentRefundInfo = new MatrixxPaymentRefundInfo();
                matrixxPaymentRefundInfo.dollarSymbol = label.VisibleRequestRefundService;
                matrixxPaymentRefundInfo.SubscriberExternalId = acc.GlobalKey__c;
                matrixxPaymentRefundInfo.refundServiceOrderInfo = RefundServiceOrderInfo;
                
                String jsonObj = JSON.serialize(matrixxPaymentRefundInfo);
                jsonObj = jsonObj.replace(label.dollarSymbol,'$');
                //system.debug('jsonObj::'+jsonObj);
                
                Http http = new Http();
                HttpRequest httpReq = new  HttpRequest();
                httpReq.setEndpoint(label.Label_targetURL);
                httpReq.setMethod(label.label_POST);
                httpReq.setHeader(label.label_Content_Type, label.label_application_json);
                httpReq.setBody(jsonObj);
                //System.debug('REQUEST---------->'+httpReq);
                //System.debug('REQUESTbody---------->'+httpReq.getbody());
                
                HttpResponse httpRes;
                //httpRes = http.send(httpReq);
                if(!test.isRunningTest()){
                    httpRes = http.send(httpReq);
                }
                else{
                    
                     MockHttpResponseGenerator mockHttpResponseGenerator =new MockHttpResponseGenerator();
                     HttpRequest reqt= new HttpRequest();
                     httpRes =mockHttpResponseGenerator.respond(reqt);
                     httpRes.setHeader('Content-Type', 'application/json');
                     httpRes.setBody('{"Result": 11,"_resultCode": 0}');
                     httpRes.setStatusCode(200);
                    
                }
                //System.debug('ResPonse----------------->'+httpRes);
                //System.debug('ResPonseBODY----------------->'+httpRes.getBody());
                //PaymentRefundResponse response;
                PaymentRefundResponse response = PaymentRefundResponse.parse(httpRes.getBody());
                if(httpRes.getStatusCode() == 200){
                    if(Integer.valueOf(label.Label_0)==response.x_resultCode){
                        // If 'Result' in PaymentRefundResponse is not equal to '0'
                        if(response.Result != label.Number_zero){
                            
                            returnobj.message = response.ResultText;
                            returnobj.statuscode = label.Label_400;
                            res.addHeader(label.label_Content_Type, label.label_application_json);
                            res.responseBody = Blob.valueOf(Json.serialize(returnObj)); 
                            res.StatusCode = 400 ; 
                            return returnObj;
                        }
                        
                        // If 'result' in PaymentRefundResponse is '0' 
                        if(response.Result == label.Number_zero){
                            List<Orderitem> unlimitedPlanOli = [select id,Refunded__c from orderitem where (Order.ordernumber =:ordNumber OR OrderId =: ordNumber) AND Product2.ProductCode =: label.Visible_Unlimited limit 1];
                            if(unlimitedPlanOli != null && unlimitedPlanOli.size() >0 ){
                                unlimitedPlanOli[0].Refunded__c = true;
                                update unlimitedPlanOli;
                            }
                            returnobj.message = response.ResultText;
                            returnobj.statuscode = label.Label_201;
                            res.addHeader(label.label_Content_Type, label.label_application_json);
                            res.responseBody = Blob.valueOf(Json.serialize(returnObj)); 
                            res.StatusCode = 201 ; 
                            return returnObj;
                            
                        }
                    }else{
                        returnobj.message = response.x_resultText;
                        returnobj.statuscode = label.Label_400;
                        res.addHeader(label.label_Content_Type, label.label_application_json);
                        res.responseBody = Blob.valueOf(Json.serialize(returnObj)); 
                        res.StatusCode = 400 ; 
                        return returnObj;
                    }
                }else{
                returnobj.message = label.Invalid_order;
                returnobj.statuscode = label.Label_400;
                res.addHeader(label.label_Content_Type, label.label_application_json);
                res.responseBody = Blob.valueOf(Json.serialize(returnObj)); 
                res.StatusCode = 400; 
                return returnObj;
                }
            }else{
                returnobj.message = label.Order_Not_Found;
                returnobj.statuscode = label.Label_400;
                res.addHeader(label.label_Content_Type, label.label_application_json);
                res.responseBody = Blob.valueOf(Json.serialize(returnObj)); 
                res.StatusCode = 400; 
                return returnObj;
            }
                
        }catch(Exception ex){
            returnobj.message = label.Label_INTERNAL_SERVER_ERROR + ex.getmessage();
            returnobj.statuscode = label.Label_500;
            res.addHeader(label.label_Content_Type, label.label_application_json);
            res.responseBody = Blob.valueOf(Json.serialize(returnObj)); 
            res.StatusCode = 500 ; 
            return returnObj;
        }
        
        returnobj.message = label.Label_Something_went_wrong;
        returnobj.statuscode = label.Label_400;
        res.addHeader(label.label_Content_Type, label.label_application_json);
        res.responseBody = Blob.valueOf(Json.serialize(returnObj)); 
        res.StatusCode = 400 ;
        return returnObj;
        */
    }
    
    global class ReturnClass{
        public String message ;
        public String statusCode;
    }
     
    public class MatrixxPaymentRefundInfo{
    public String dollarSymbol;
    public String SubscriberExternalId;
    public RefundServiceOrdrInfo RefundServiceOrderInfo;
    }
     
    public class RefundServiceOrdrInfo {
        public String dollarSymbol;
        public String ReturnOrderId;
        public String OriginalOrderId;
        public String ServiceOfferExternalId;
        public String OriginalOrderDate;
    }
    /*
    public class MockResponseForPositiveScenario implements HttpCalloutMock {
        
    public HTTPResponse respond(HTTPRequest req) {
        
        
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"$": "VisibleResponseRefundService","Result": "0","ResultText": "<success information> | <reason if failure>","RefundedAmount": "<amount refunded>","RefundAmountInfo": "<information about refund amount>","_resultCode": 0 ,"_resultText": "OK","_resultType": "type"}');
        res.setStatusCode(200);
        return res;
    }
   } */
    public class applicationException extends Exception {}
}